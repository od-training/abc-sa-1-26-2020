# Links

Links to various documentation/helpful learning sites

- RxJS
  https://rxmarbles.com/
  https://www.learnrxjs.io/

  - Book
    Build Reactive Websites with RxJS: Master Observables and Wrangle Events (Randall Koutnik)

- Angular
  https://github.com/angularbootcamp/abc
  https://angular.io/docs

- Typescript
  https://www.typescriptlang.org/docs/

- General Web (html, js, css)
  https://developer.mozilla.org/en-US/docs/Web

- Extensions
  https://augury.angular.io/
  [Redux Extension](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)

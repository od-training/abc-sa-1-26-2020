import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Video } from '../../types';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  getVideos() {
    return this.http.get<Video[]>('https://api.angularbootcamp.com/videos', {
      responseType: 'json'
    });
  }
}

import { Component, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {
  Router,
  ActivatedRouteSnapshot,
  ActivatedRoute
} from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnDestroy {
  filtersForm: FormGroup;
  regionOptions = [
    { value: 'west', label: 'West' },
    { value: 'east', label: 'East' }
  ];

  destroy = new Subject();

  constructor(router: Router, route: ActivatedRoute) {
    this.filtersForm = new FormGroup({
      title: new FormControl(route.snapshot.queryParamMap.get('title') || ''),
      author: new FormControl(''),
      region: new FormControl('east')
    });

    this.filtersForm
      .get('title')
      .valueChanges.pipe(takeUntil(this.destroy))
      .subscribe(title => {
        router.navigate([], {
          queryParams: {
            title
          },
          queryParamsHandling: 'merge'
        });
      });
  }

  ngOnDestroy() {
    this.destroy.next();
    this.destroy.complete();
  }
}

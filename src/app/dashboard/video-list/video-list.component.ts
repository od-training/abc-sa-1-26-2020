import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Video } from '../../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  @Input() selectedVideoId?: string | null;

  constructor(private router: Router) {}

  selectVideo(video: Video) {
    let videoId: string | null = null;

    if (video.id !== this.selectedVideoId) {
      videoId = video.id;
    }

    this.router.navigate([], {
      queryParams: {
        videoId
      },
      queryParamsHandling: 'merge'
    });
  }
}

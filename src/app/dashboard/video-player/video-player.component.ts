import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {
  @Input() set videoId(id: string | null) {
    if (id) {
      const url = 'https://www.youtube.com/embed/' + id;

      this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    } else {
      this.videoUrl = null;
    }
  }

  videoUrl: SafeResourceUrl | undefined | null;

  constructor(public sanitizer: DomSanitizer) {}
}

import { Component } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Video } from 'src/app/types';
import { VideoDataService } from '../../services/videos/video-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;
  currentVideoId: Observable<string | null>;

  constructor(vds: VideoDataService, route: ActivatedRoute, router: Router) {
    const fetchVideos = vds.getVideos().pipe(
      tap(videos => {
        if (!route.snapshot.queryParamMap.get('videoId')) {
          const videoId = videos[0].id;
          router.navigate([], {
            queryParams: {
              videoId
            },
            queryParamsHandling: 'merge'
          });
        }
      })
    );

    const titleFilter = route.queryParamMap.pipe(
      map(params => params.get('title') || '')
    );

    const filteredVideos = combineLatest([fetchVideos, titleFilter]).pipe(
      map(([videos, title]) => {
        return videos.filter(video => {
          return video.title.toLowerCase().startsWith(title.toLowerCase());
        });
      })
    );

    this.videoList = filteredVideos.pipe(
      map(videos => {
        return videos.map(video => {
          return {
            ...video,
            title: video.title.toUpperCase()
          };
        });
      })
    );

    this.currentVideoId = route.queryParamMap.pipe(
      map(params => params.get('videoId'))
    );
  }
}
